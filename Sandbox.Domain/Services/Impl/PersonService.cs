﻿using Sandbox.Domain.Services.Interfaces;

namespace Sandbox.Domain.Services.Impl
{
	public class PersonService : IPersonService
	{
		public string Url { get; set; }
	}
}