﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Sandbox.Domain
{	
	public class User : IDisposable
	{
		[Required]
		public int Id { get; set; }
		public string UserName { get; private set; }
		public int UserAge { get; private set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string Zip { get; set; }
		public string Address2 { get; set; }
		public int Age { get; private set; }
		public ICollection<string> Jobs { get; set; }

		public ICollection<string> Educations { get; set; }
		public ICollection<string> Skills { get; set; }


		public User()
		{
			Age = 10;
			Educations = new Collection<string>();
			Skills = new Collection<string>();
		}

		public void Dispose()
		{
			Educations = new Collection<string>();
			Skills = new Collection<string>();
			Jobs = new Collection<string>();
		}
	}
}
